
from driver import PENDING_RESULTS, FrameworkDriver, PENDING_ALERTS
import asyncio



async def run_test(driver):
    """Run all unit tests"""

    PENDING_ALERTS.put("This is a test of alert to screen")
    PENDING_ALERTS.put("a bunch of gibbering nonsense")
    PENDING_ALERTS.put("this is the last a final nonsense")
    await asyncio.sleep(10)
    PENDING_RESULTS.put("THIS IS TEST FOR RESULTS")
    PENDING_RESULTS.put("THIS IS ALSO TEST FOR RESULTS")
    PENDING_RESULTS.put("Pizza is good and stuff")
    PENDING_RESULTS.put("Always eat tacos with friends")



async def run_framework():
    """This initiliazes the application and then starts the CLI.
    Runs each as a coroutine."""
    app = FrameworkDriver()
    await asyncio.gather(app.start(), run_test(app))


def main():
    try:
        asyncio.run(run_framework())
    except KeyboardInterrupt:
        print("Received exit, exiting")


if __name__ == "__main__":
    main()