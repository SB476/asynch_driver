import queue
import asyncio
from termios import PENDIN
from unittest import result

from prompt_toolkit.patch_stdout import patch_stdout
from prompt_toolkit.shortcuts.prompt import PromptSession

from concurrent.futures import ThreadPoolExecutor

import time
import multiprocessing

#QUEUES:
PENDING_TASKS = queue.Queue()
PENDING_IO = queue.Queue()
PENDING_ALERTS = queue.Queue()
PENDING_RESULTS = queue.Queue()


class FrameworkDriver():
    """This is the main class that invokes the framework event loop. The event loops process:

    Proccess Queues
    Display prompt
    """

    def __init__(self, max_threads: int = multiprocessing.cpu_count()) -> None:
        """Initiliaze the relavent information for the framework"""

        # self.max_threads = queue.Queue(max_threads)
        self.max_treads = max_threads

    async def _pre_loop_hook(self):
        """Place holder for pre hook"""
        pass


    async def proccess_queues(self):

        try:
            while True:
                asyncio.gather(
                    self.proccess_pending_tasks(PENDING_TASKS),
                    self.processing_pending_io(PENDING_IO),
                    self.proccess_pending_alerts(PENDING_ALERTS),
                    self.process_pending_results(PENDING_RESULTS),
                )
                await asyncio.sleep(0.5)
        except asyncio.CancelledError:
            print("Background task cancelled.")

    async def proccess_pending_tasks(self, task_queue: queue.Queue):
        """This procceses the tasks from task queue using the specified max number of threads.
        This should spawn seperate threads in order to accomplish what is expected to be high processing intensive
        functions."""

        loop = asyncio.get_running_loop()
        while not task_queue.empty():
            try:
                task = task_queue.get_nowait()
            except queue.Empty:
                pass
            else:
                await loop.run_in_executor(task.run_task)
                # print(f"Tasks left: {task_queue.qsize()}. Current task is: {task}")
                task_queue.task_done()

    async def process_pending_results(self, result_queue):

        result_from_queue = []
        while not result_queue.empty():
            result_from_queue.append(result_queue.get())

        print(f"Results are: {result_from_queue}")


    async def processing_pending_io(self, io_queue:queue.Queue):
        # print("Proccessing queue: I/O")
        while io_queue.qsize() > 0:
            try:
                io_object = io_queue.get_nowait()
                # If `False`, the program is not blocked. `Queue.Empty` is thrown if
                # the queue is empty
            except queue.Empty:
                io_object = None
            else:
                io_queue.task_done()
            
                print(io_object)

    async def proccess_pending_alerts(self, alert_queue):
        # print("Proccessing queue: Alerts")
        if alert_queue.qsize() > 0:
            try:
                alert_message = alert_queue.get_nowait()
            except queue.Empty:
                alert_message = None
            else:
                alert_queue.task_done()

            print(alert_message)

    async def display_prompt(self):
        """Display something to user"""
        # Create Prompt.
        session = PromptSession("Say something: ")

        # Run echo loop. Read text from stdin, and reply it back.
        while True:
            try:
                result = await session.prompt_async()
                PENDING_RESULTS.put('You said: "{0}"'.format(result))
            except (EOFError, KeyboardInterrupt):
                return

    async def _post_loop_hook(self):
        """Place holder for post hook"""
        pass

    async def start(self):
        """This begines the event loop. Coroutines that desire to be run should be put here."""

        with patch_stdout():
            proccess_task = asyncio.create_task(self.proccess_queues())
            try:
                await asyncio.gather(
                    self._pre_loop_hook(),
                    self.display_prompt(),
                    self._post_loop_hook()
                )
            finally:
                proccess_task.cancel()
