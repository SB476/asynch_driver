# unique_queue_results

This is a concept project that addresses issue with asynchronously processing information across multiple threads.

## Description
This is a concept project about creating a multi-threaded asynchronous application. It makes use
of prompt_toolkit becuase...well I think it's  a really powerfull library and I like the way it looks. Intgration with 
asynchio is very easy to use and fantastic.

## The Issue
For many of my projects I found myself wanting to use asynch and threading to speed up processing. This framework conceptializes how to createa a multi-threaded asynchronous application.

## The Solution/Concept
The system relies on a "driver" that proccesses all the queues. For some queues or tasks a seperate thread is launched in order to handle the blocking or more intensive computations.

## Usage
This project makes use of poetry. Download and install if not already.

`poetry install`
`poetry shell`
`python3 unqiue_queue_results`